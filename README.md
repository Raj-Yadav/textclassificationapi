# FastAPI Text Classification API Documentation

This documentation provides details about the Text Classification API developed using FastAPI. The API is designed to classify the sentiment of text inputs using a Hugging Face model.

## Modules and Files

### `main.py`

Contains the main FastAPI application with the endpoint for classifying text sentiment.

## Endpoint

### Classify Sentiment

- **URL:** `/`
- **Method:** `GET`
- **Description:** Classifies the sentiment of the provided text input.
- **Parameters:**
  - `payload`: The text input to be classified.
- **Response:** Returns a JSON object containing the classification result.

## Dependencies

### Configuration

The API requires certain configurations stored in a separate `config.py` file. These configurations include the API URL and headers required for making requests to the sentiment classification service.

## Functionality

### Sentiment Classification

The API utilizes a Hugging Face model to classify the sentiment of text inputs. It sends a request to the sentiment classification service with the provided text input and returns the classification result.

### Request Format

The text input to be classified is provided as a query parameter `payload` in the GET request to the `/` endpoint.

### Response Format

The API returns a JSON object containing the classification result. The result typically includes information such as the sentiment label and confidence scores.

## Example

### Request

```http
GET /?payload=I%20like%20you.%20I%20love%20you 

### Response

{
  "sentiment": "positive",
  "confidence": 0.85
}
