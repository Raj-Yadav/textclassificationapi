from fastapi import FastAPI
import requests
import config

app = FastAPI()

API_URL = config.API_URL
headers = config.headers

def query(payload):
	response = requests.post(API_URL, headers=headers, json=payload)
	return response.json()
	
output = query({
	"inputs": "I like you. I love you",
})

@app.get("/")
async def classify_sentiment(payload: str):
    result = query(payload)
    return result